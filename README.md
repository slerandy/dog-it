# Dawg

Cette application recence des recettes de nourriture pour chien grâce à l'API Rest Recipe Puppy

### Installing

Dans un dossier vide faire :
- Clique droit > Git Bash here
```
git init
git clone https://gitlab.com/Shiizzux/dog-it.git
```

## Authors

**Samuel Lérandy**

## Generalities 
 
- Number of activities : 2
- Splash screen and customised app icon : yes
- Pattern : Singleton
- API Rest : [Recipe Puppy](http://www.recipepuppy.com/about/api/)
           : [Dog Api](https://dog.ceo/dog-api/documentation/)
- Data save : SharedPreferences

Utilisation of :
- Intent
- RecyclerView
- Listeners

## External Librairies 

[Picasso](https://square.github.io/picasso/#introduction)
[Retrofit](https://github.com/square/retrofit)

## Presentation 
*The Recipe Puppy API sends low quality images*

* Main screen

<img src="screens/main%20screen.jpg" width="200" >

* Detail screen

<img src="screens/details%20screen.jpg" width="200" >




