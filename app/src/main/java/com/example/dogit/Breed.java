package com.example.dogit;

import java.util.ArrayList;
import java.util.List;

public class Breed {
    private String name;
    private List<Dog> dogs;

    public Breed(String name) {
        this.name = name;
        this.dogs = new ArrayList<>();
    }

    public void addDog(Dog dog){
        dogs.add(dog);
    }
}
