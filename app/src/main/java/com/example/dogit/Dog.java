package com.example.dogit;

public class Dog {
    private String name;
    private String imgName;

    public Dog(String name, String imgName) {
        this.name = name;
        this.imgName = imgName;
    }

    public String getName() {
        return name;
    }

    public String getImgName() {
        return imgName;
    }
}
