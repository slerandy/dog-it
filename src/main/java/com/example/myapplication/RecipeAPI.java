package com.example.myapplication;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Url;

public interface RecipeAPI {
    @GET("/api")
    Call <RecipeValue.RecipeResult> fetchRecipes();
}
