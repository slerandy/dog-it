package com.example.myapplication;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.thekhaeng.recyclerviewmargin.LayoutMarginDecoration;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {
    public static final String FAVORITES = "FAVORITES";
    private static final String RANDOM_DOG_URL = "https://images.dog.ceo/breeds/appenzeller/n02107908_7443.jpg";

    public static SharedPreferences saves;
    private static RecyclerView.Adapter adapter;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layManager;
    private App app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);

        // hide toolbar
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar

        // set fullscreen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //show the activity in full screen

        setContentView(R.layout.activity_main);

        // instance of app
        app = App.getInstance();

        // load main image
        Picasso.get().load(RANDOM_DOG_URL).into((ImageView) findViewById(R.id.imgViewRandomDog));

        recyclerView = (RecyclerView) findViewById(R.id.recyViewBreeds);
        // space between cells & padding
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.cellsSpacing);
        LayoutMarginDecoration layoutMargin = new LayoutMarginDecoration(2, spacingInPixels);
        layoutMargin.setPadding(recyclerView, 16, 0, 8, 8);
        recyclerView.addItemDecoration(layoutMargin);

        // layout manager
        layManager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layManager);

        RecipeController controller = new RecipeController();
        controller.start();
        controller.fetchRecipes();

        adapter = new RecipeAdapter(app.getRecipes());
        recyclerView.setAdapter(adapter);

        // sharedPreferences
        saves = getBaseContext().getSharedPreferences(FAVORITES, MODE_PRIVATE);
    }

    public static void notifyAdapter(int insertIndex, int size) {
        adapter.notifyItemRangeInserted(insertIndex, size);
    }

    public static boolean isFavorite(String title){
        if (saves.contains(title))
            return saves.getBoolean(title, false);
        else {
            saves.edit().putBoolean(title, false).apply();
            return false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }
}
