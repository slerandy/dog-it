package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class RecipeDetailsActivity extends AppCompatActivity {
    public static final String FAVORITES = "FAVORITES";
    private App app;
    private RecipeValue recipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // hide toolbar
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar

        // set fullscreen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //show the activity in full screen

        setContentView(R.layout.activity_recipe_details);

        // get parameter from intent
        this.app = App.getInstance();
        this.recipe = app.getRecipe(getIntent().getStringExtra("TITLE"));

        // load thumbnail
        ImageView imgView = findViewById(R.id.imgViewRecipe);
        Picasso.get().load(recipe.getThumbnail()).into(imgView);

        // add title
        ((TextView) findViewById(R.id.txtViewTitle)).setText(recipe.getTitle());

        // add ingredients
        ((TextView) findViewById(R.id.txtViewIngredients)).setText(recipe.getIngredients());

        // load favorite
        String img = recipe.isFavorite() ? "@drawable/favorite" : "@drawable/add_favorite";
        int id = getResources().getIdentifier(img, null, getPackageName());
        ((ImageView) findViewById(R.id.imgViewFavorite)).setImageResource(id);
    }

    public void onClick(View v) {
        super.onBackPressed();
    }

    public void goToHref(View view) {
        goToUrl(recipe.getHref());
    }

    private void goToUrl(String url) {
        Uri uriUrl = Uri.parse(url);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    public void swapFavorite(View v) {
        recipe.setFavorite(!recipe.isFavorite());
        SharedPreferences saves = getSharedPreferences(FAVORITES, MODE_PRIVATE);
        saves.edit().remove(recipe.getTitle()).putBoolean(recipe.getTitle(), recipe.isFavorite()).apply();

        int imageResource;
        String text;

        if (recipe.isFavorite()) {
            imageResource = getResources().getIdentifier("@drawable/favorite", null, getPackageName());
            text = "Recette ajoutée à vos favoris.";
        } else {
            imageResource = getResources().getIdentifier("@drawable/add_favorite", null, getPackageName());
            text = "Recette enlevée de vos favoris.";
        }
        ((ImageView) v).setImageResource(imageResource);
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }
}
