package com.example.myapplication;

import java.util.ArrayList;
import java.util.List;

public class App {
    private static App app;
    private List<RecipeValue> recipes;

    private App() {
        this.recipes = new ArrayList<>();
    }

    public static App getInstance() {
        return app = app == null ? new App() : app;
    }

    public List<RecipeValue> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<RecipeValue> recipes) {
        app.recipes = recipes;
    }

    public void addRecipe(RecipeValue recipe) {
        app.recipes.add(recipe);
    }

    public RecipeValue getRecipe(String title) {
        for (RecipeValue value : recipes)
            if (value.getTitle().equals(title))
                return value;
        return null;
    }
}
