package com.example.myapplication;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Outline;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static com.example.myapplication.MainActivity.isFavorite;

public class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.ViewHolder> {
    private List<RecipeValue> values;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTitle;
        public ImageView imgThumb;
        public View layout;
        public ImageView imgFavorite;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            txtTitle = (TextView) v.findViewById(R.id.txtViewTitle);
            imgThumb = (ImageView) v.findViewById(R.id.imgViewThumbnail);
            imgFavorite = (ImageView) v.findViewById(R.id.imgViewFavorite);

            // listener to open details activity
            v.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, RecipeDetailsActivity.class);

                    // get parameters for details
                    String title = (String) txtTitle.getText();
                    intent.putExtra("TITLE", title);
                    context.startActivity(intent);
                }
            });
        }
    }


    public void add(int position, RecipeValue item) {
        values.add(position, item);
        Log.d("ADAPTER", item.toString());
        notifyItemInserted(position);
    }

    public void remove(int position) {
        values.remove(position);
        notifyItemRemoved(position);
    }

    public RecipeAdapter(List<RecipeValue> dataSet) {
        values = dataSet;
    }

    @Override
    public RecipeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.recipe_adapter, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeAdapter.ViewHolder holder, final int position) {
        final RecipeValue value = values.get(position);
        holder.txtTitle.setText(value.getTitle());
        Picasso.get().load(value.getThumbnail()).into(holder.imgThumb);

        ViewOutlineProvider provider = new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                int curveRadius = 24;
                outline.setRoundRect(0, 0, view.getWidth(), (view.getHeight()+curveRadius), curveRadius);
            }
        };
        holder.imgThumb.setOutlineProvider(provider);
        holder.imgThumb.setClipToOutline(true);

        value.setFavorite(MainActivity.isFavorite(value.getTitle()));
        if (value.isFavorite())
            holder.imgFavorite.setVisibility(View.VISIBLE);
        else
            holder.imgFavorite.setVisibility(View.INVISIBLE);

        Log.d("RECYCLER_VIEW", "new item : " + value.getTitle() + " favorite : " + value.isFavorite());
    }

    @Override
    public int getItemCount() {
        return values.size();
    }
}

