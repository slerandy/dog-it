package com.example.myapplication;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RecipeController implements Callback<RecipeValue.RecipeResult> {
    private static final String BASE_URL = "http://www.recipepuppy.com/";
    private RecipeAPI recipeAPI;


    public void start() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();

        recipeAPI = retrofit.create(RecipeAPI.class);
    }

    public void fetchRecipes(){
        Call<RecipeValue.RecipeResult> call = recipeAPI.fetchRecipes();
        call.enqueue(this);
    }


    @Override
    public void onResponse(Call<RecipeValue.RecipeResult> call, Response<RecipeValue.RecipeResult> response) {
        if (!response.isSuccessful()) {
            System.out.println(response.body());
            return;
        }
        List<RecipeValue> recipes = response.body().getRecipes();
        App app = App.getInstance();
        for (RecipeValue recipe : recipes) app.addRecipe(recipe);
        MainActivity.notifyAdapter(0, recipes.size());
    }

    @Override
    public void onFailure(Call<RecipeValue.RecipeResult> call, Throwable t) {
        t.printStackTrace();
    }
}
